#!/bin/bash
##
# "THE BEER-WARE LICENSE" (Revision 42):
# <mail@philip-henning.com> wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return Philip Henning.
##

## Configuration
usr="" #User which runs znc. E.g. "znc"
cfgpath="" #Path to znc config. Usually $HOME/.znc of the user which runs znc. E.g. "/home/znc/.znc"
domain="" #Let's encrypt certificate domain.
dhparam="" #Optional: path to custom dhparam. E.g. "/home/znc/.znc/dhparam.pem"

## Do net edit anything below!
if [[ $UID -ne 0 ]]; then
	echo "You have to run this script as root!"
	exit 1
fi

if [[ "$1" == "force" ]]; then
	force=true
else
	force=false
fi

## Get last modified time
lastmod="$(stat -c %Y /etc/letsencrypt/live/$domain/fullchain.pem)"

## Renew certificate
/usr/bin/letsencrypt renew > /dev/null

## Compare modified date and renew certificate
if [[ $(stat -c %Y /etc/letsencrypt/live/$domain/fullchain.pem) -ne $lastmod ]] || [[ $force == true ]]; then
	cp -pf $cfgpath/znc.pem $cfgpath/znc.pem.bak
	echo "" > $cfgpath/znc.pem
	cat /etc/letsencrypt/live/$domain/privkey.pem > $cfgpath/znc.pem
	cat /etc/letsencrypt/live/$domain/cert.pem >> $cfgpath/znc.pem
	cat /etc/letsencrypt/live/$domain/chain.pem >> $cfgpath/znc.pem
	if [[ -n $dhparam ]]; then
		cat $dhparam >> $cfgpath/znc.pem
	fi
fi
exit 0
