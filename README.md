# Renew script for [Let's Encrypt](https://letsencrypt.org/) Certificates for [ZNC](http://wiki.znc.in/ZNC)
This is a little script to renew your Let's Encrypt certificate for ZNC.

## Requirements
- Let's Encrypt commandlline utility.
- This script is based on Ubuntu 16.04 LTS
- ZNC
- Obviously ZNC

## How to use
You have to run this script as root!
Just save the script, make it executable and add a cronjob.
If you want to force the copying run "./renew\_znc\_le\_cert.sh force"
